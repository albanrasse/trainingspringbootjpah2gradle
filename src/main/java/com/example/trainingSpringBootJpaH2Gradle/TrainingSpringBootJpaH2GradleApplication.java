package com.example.trainingSpringBootJpaH2Gradle;

import com.example.trainingSpringBootJpaH2Gradle.model.Company;
import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.*;
import com.example.trainingSpringBootJpaH2Gradle.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.OneToOne;
import java.util.List;

@SpringBootApplication
public class TrainingSpringBootJpaH2GradleApplication implements CommandLineRunner {

	@Autowired
	CompanyServiceImpl companyServiceImpl;
	@Autowired
	ColorServiceImpl colorServiceImpl;
	@Autowired
	SizeServiceImpl sizeServiceImpl;
	@Autowired
	ItemMarketingServiceImpl itemMarketingServiceImpl;
	@Autowired
	ItemServiceImpl itemServiceImpl;
	@Autowired
	VendorServiceImpl vendorServiceImpl;

	public static void main(String[] args) {
		SpringApplication.run(TrainingSpringBootJpaH2GradleApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("*********** START APP BRANCH MASTER  ************");
		insertElements();
	}

	private void insertElements(){

		// REMOVE ALL
		List<Company> companies = companyServiceImpl.findAll();
		List<Color> colors = colorServiceImpl.findAll();
		List<Size> sizes = sizeServiceImpl.findAll();

		for(int i=0; i<companies.size(); i++){ companyServiceImpl.delete(companies.get(i)); }
		for(int i=0; i<colors.size(); i++){ colorServiceImpl.delete(colors.get(i)); }
		for(int i=0; i<sizes.size(); i++){ sizeServiceImpl.delete(sizes.get(i)); }

		// INSERT COMPANIES
		Company company1 = new Company("company1");
		Company company2 = new Company("company2");
		Company company3 = new Company("company3");
		Company company4 = new Company("company4");
		Company company5 = new Company("company5");

		companyServiceImpl.save(company1);
		companyServiceImpl.save(company2);
		companyServiceImpl.save(company3);
		companyServiceImpl.save(company4);
		companyServiceImpl.save(company5);

		// INSERT COLORS
		Color color1 = new Color("B", "bleu");
		Color color2 = new Color("R", "rouge");
		Color color3 = new Color("V", "vert");
		Color color4 = new Color("J", "jaune");

		colorServiceImpl.save(color1);
		colorServiceImpl.save(color2);
		colorServiceImpl.save(color3);
		colorServiceImpl.save(color4);

		// INSERT SIZES
		Size size1 = new Size("S", "32");
		Size size2 = new Size("L", "36");
		Size size3 = new Size("M", "38");
		Size size4 = new Size("XL", "40");

		sizeServiceImpl.save(size1);
		sizeServiceImpl.save(size2);
		sizeServiceImpl.save(size3);
		sizeServiceImpl.save(size4);

		// INSERT ITEM MARKETING
		ItemMarketing im1 = new ItemMarketing(1, color1, size1);
		ItemMarketing im2 = new ItemMarketing(2, color2, size1);
		ItemMarketing im3 = new ItemMarketing(3, color2, size1);
		ItemMarketing im4 = new ItemMarketing(4, color2, size1);

		itemMarketingServiceImpl.save(im1);
		itemMarketingServiceImpl.save(im2);
		itemMarketingServiceImpl.save(im3);
		itemMarketingServiceImpl.save(im4);

		// INSERT VENDOR
		Vendor v1 = new Vendor("v1", "vendor 1");
		Vendor v2 = new Vendor("v2", "vendor 2");
		Vendor v3 = new Vendor("v3", "vendor 3");
		Vendor v4 = new Vendor("v3", "vendor 4");

		vendorServiceImpl.save(v1);
		vendorServiceImpl.save(v2);
		vendorServiceImpl.save(v3);
		vendorServiceImpl.save(v4);

		// INSERT ITEM
		Item item1 = new Item("axItem1", "as400Item1", "productName1", im1, v1);
		Item item2 = new Item("axItem2", "as400Item2", "productName2", im2, v2);
		Item item3 = new Item("axItem3", "as400Item3", "productName3", im3, v3);
		Item item4 = new Item("axItem4", "as400Item4", "productName4", im4, v4);

		itemServiceImpl.save(item1);
		itemServiceImpl.save(item2);
		itemServiceImpl.save(item3);
		itemServiceImpl.save(item4);


		ItemMarketing im5 = new ItemMarketing(5, color1, size1, item3, item4);
		itemMarketingServiceImpl.save(im5);


		/*

SELECT ITEM_MARKETING.replacement_item_id, ITEM_MARKETING.substitute_item_id, ITEM.product_name as product_name  FROM ITEM_MARKETING
INNER JOIN ITEM ON ITEM.id = ITEM_MARKETING.replacement_item_id

		SELECT ITEM.ax_item_id as ax, ITEM_MARKETING.indicator as indicator, Color.label as color, Size.label as size From ITEM
		INNER JOIN ITEM_MARKETING ON ITEM.item_marketing_id = ITEM_MARKETING.id
		INNER JOIN Color ON ITEM_MARKETING.color_id = Color .id
		INNER JOIN Size ON ITEM_MARKETING.size_id = Size .id

		SELECT indicator, Color.label as color, Size.label as size FROM ITEM_MARKETING
		INNER JOIN Color ON ITEM_MARKETING.color_id = Color .id
		INNER JOIN Size ON ITEM_MARKETING.size_id = Size .id

		SELECT ITEM_MARKETING.indicator, ITEM.productName as productName , ITEM.substituteItem as substituteItem FROM ITEM_MARKETING
		INNER JOIN ITEM ON ITEM_MARKETING.replacement_item_id = ITEM.id
		INNER JOIN ITEM ON ITEM_MARKETING.substitute_item_id = ITEM.id

		*/

	}


}
