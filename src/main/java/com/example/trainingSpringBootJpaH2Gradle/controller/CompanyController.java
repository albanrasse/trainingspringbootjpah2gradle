package com.example.trainingSpringBootJpaH2Gradle.controller;

import com.example.trainingSpringBootJpaH2Gradle.model.Company;
import com.example.trainingSpringBootJpaH2Gradle.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("company")
public class CompanyController {

    /*
    @RestController is a specialized version of the controller.
    It includes the @Controller and @ResponseBody annotations,
    and as a result, simplifies the controller implementation:

    The controller is annotated with the @RestController annotation;
    therefore, the @ResponseBody isn't required.

    Every request handling method of the controller class automatically
    serializes return objects into HttpResponse.

    */


    @Autowired
    private CompanyService companyService;

    @GetMapping( value="", produces = "application/json")
    public @ResponseBody String getBook() {
        return "TEST API OK";
    }

    @GetMapping(value="getCompany/{id}", produces = "application/json")
    public @ResponseBody Company getCompany(@PathVariable int id) {
        return companyService.getCompany(id);
    }

    @GetMapping(value="getCompanies", produces = "application/json")
    public List<Company> getCompanies(){
        List<Company> companies = companyService.getCompanies();
        System.out.println("companies : " + companies);

        List<Company> companyForTest = new ArrayList<Company>();
        companyForTest.add(new Company());
        companyForTest.add(new Company());
        companyForTest.add(new Company());
        return companyForTest;
    }

    // TODO
    // remove
    // update
    // add


}
