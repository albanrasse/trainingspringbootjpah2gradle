package com.example.trainingSpringBootJpaH2Gradle.controller;


import com.example.trainingSpringBootJpaH2Gradle.model.Company;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("test")
public class TrainingController {

    //GET
    //http://localhost:8080/test/
    //TEST API test
    @GetMapping( value="/")
    public @ResponseBody String test() {
        return "TEST API test";
    }

    //GET
    //http://localhost:8080/test/test1
    //TEST API test1
    @GetMapping( value="/test1")
    public @ResponseBody String test1() {
        return "TEST API test1";
    }

    //GET
    //http://localhost:8080/test/test2
    //TEST API test2
    @GetMapping( value="/test2", produces = "application/json")
    public @ResponseBody String test2() {
        return "TEST API test2";
    }

    //GET
    //http://localhost:8080/test/test3
    //{
    //    "timestamp": "2022-02-27T17:33:18.184+00:00",
    //        "status": 415,
    //        "error": "Unsupported Media Type",
    //        "path": "/test/test3"
    //}
    @GetMapping( value="/test3",
            consumes = "application/json",
            produces = "application/json"
    )
    public @ResponseBody String test3() {
        return "TEST API test3";
    }

    //GET
    //http://localhost:8080/test/test4/unString
    //TEST API test4 unString
    @GetMapping(value="test4/{monString}")
    public @ResponseBody String test4(@PathVariable String monString) {
        return "TEST API test4 " + monString;
    }


    //GET
    //http://localhost:8080/test/test5/unString
    //TEST API test5 unString
    @GetMapping(value="test5/{monString}", produces = "application/json")
    public @ResponseBody String test5(@PathVariable String monString) {
        return "TEST API test5 " + monString;
    }

    //GET
    //http://localhost:8080/test/test6/unString
    //{
    //    "timestamp": "2022-02-27T17:40:51.310+00:00",
    //        "status": 415,
    //            "error": "Unsupported Media Type",
    //            "path": "/test/test6/unString"
    //}
    @GetMapping(value="test6/{monString}",
            consumes = "application/json",
            produces = "application/json")
    public @ResponseBody String test6(@PathVariable String monString) {
        return "TEST API test6 " + monString;
    }

    //POST
    //http://localhost:8080/test/test61
    //{
    //    "id": 0,
    //    "name": null
    //}
    @PostMapping(value="/test61")
    public @ResponseBody
    Company test61() {
        return new Company();
    }


    //POST
    //http://localhost:8080/test/test62
    //{
    //    "id": 0,
    //    "name": null
    //}
    @PostMapping(value="/test62")
    public @ResponseBody
    Company test62() {
        return new Company();
    }

    //POST
    //http://localhost:8080/test/test63
    //{
    //    "id": 0,
    //    "name": null
    //}
    @PostMapping(value="/test63",
    produces = "application/json")
    public @ResponseBody
    Company test63() {
        return new Company();
    }

    //POST
    //http://localhost:8080/test/test64
    //{
    //    "timestamp":"2022-02-27T18:00:26.645+00:00",
    //        "status":415,
    //        "error":"Unsupported Media Type",
    //        "path":"/test/test63"
    //}
    @PostMapping(value="/test64",
            consumes = "application/json",
            produces = "application/json")
    public @ResponseBody
    Company test64() {
        return new Company();
    }

    //POST
    //http://localhost:8080/test/test7
    //{
    //    "company": "macompany"
    //}

    //{
    //    "company": "macompany"
    //}
    @PostMapping(value="/test7")
    public @ResponseBody
    String test7(@RequestBody String company) {
        return company;
    }


    //POST
    //http://localhost:8080/test/test71
    //{
    //    "id": "macompany",
    //    "name : "mon nom"
    //}

    //{
    //    "id": "macompany",
    //    "name : "mon nom"
    //}
    @PostMapping(value="/test71",
            consumes = "application/json",
            produces = "application/json")
    public @ResponseBody
    String test71(@RequestBody String company) {
        return company;
    }

    //POST
    //http://localhost:8080/test/test72
    //{
    //    "id": 1,
    //    "name : "mon nom"
    //}

    //{
    //    "id": 1,
    //    "name : "mon nom"
    //}
    @PostMapping(value="/test72",
            consumes = "application/json",
            produces = "application/json")
    public @ResponseBody
    Company test72(@RequestBody Company company) {
        return company;
    }


    //POST
    //http://localhost:8080/test/test8
    //{
    //    "id": 1,
    //        "name" : "ma company"
    //}

    //{
    //    "id": 2,
    //    "name" : "ma company 2"
    //}
    @PostMapping(value="/test8",
            consumes = "application/json",
            produces = "application/json")
    public @ResponseBody
    Company test8(@RequestBody Company company) {
        System.out.println(company.getId() + ", " + company.getName());
        return new Company(2, "ma company 2");
    }

    //POST
    //http://localhost:8080/test/test9
    //[
    //    {
    //        "id":1,
    //            "name":"third"
    //    },
    //    {
    //        "id":2,
    //            "name":"four"
    //    },
    //    {
    //        "id":3,
    //            "name":"second"
    //    }
    //]

    //[
    //    {
    //        "id": 10,
    //            "name": "company 10"
    //    },
    //    {
    //        "id": 11,
    //            "name": "company 11"
    //    }
    //]
    @PostMapping(value="/test9",
            consumes = "application/json",
            produces = "application/json")
    public @ResponseBody
    List<Company> test9(@RequestBody List<Company> companies) {
        Company c1 = new Company(10, "company 10");
        Company c2 = new Company(11, "company 11");
        List<Company> companyList = new ArrayList<Company>();
        companyList.add(c1);
        companyList.add(c2);
        return companyList;
    }


}
