package com.example.trainingSpringBootJpaH2Gradle.repository;


import com.example.trainingSpringBootJpaH2Gradle.model.Company;
import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.Color;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ColorRepository extends CrudRepository<Color, Integer> {

}
