package com.example.trainingSpringBootJpaH2Gradle.repository;


import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.Color;
import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.ItemMarketing;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemMarketingRepository extends CrudRepository<ItemMarketing, Integer> {

}
