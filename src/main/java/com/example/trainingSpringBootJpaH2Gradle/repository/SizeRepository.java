package com.example.trainingSpringBootJpaH2Gradle.repository;


import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.Color;
import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.Size;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SizeRepository extends CrudRepository<Size, Integer> {

}
