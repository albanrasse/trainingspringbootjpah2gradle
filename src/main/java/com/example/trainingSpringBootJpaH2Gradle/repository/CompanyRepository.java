package com.example.trainingSpringBootJpaH2Gradle.repository;


import com.example.trainingSpringBootJpaH2Gradle.model.Company;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends CrudRepository<Company, Integer> {

}
