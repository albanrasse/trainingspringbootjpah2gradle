package com.example.trainingSpringBootJpaH2Gradle.service;

import com.example.trainingSpringBootJpaH2Gradle.model.Company;

import java.util.List;

public interface CompanyService {

    Company saveCompany(Company department);

    List<Company> getCompanies();

    Company getCompany(Integer id);

    Company updateCompany(Company company);

    void deleteCompany(Integer departmentId);

}
