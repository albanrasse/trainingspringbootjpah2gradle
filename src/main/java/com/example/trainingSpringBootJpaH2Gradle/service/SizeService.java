package com.example.trainingSpringBootJpaH2Gradle.service;

import com.example.trainingSpringBootJpaH2Gradle.model.Company;
import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.Size;

import java.util.List;

public interface SizeService {

    Size saveSize(Size size);

    List<Size> getSizes();

    Size getSize(Integer id);

    Size updateSize(Size size);

    void deleteSize(Integer size);

}
