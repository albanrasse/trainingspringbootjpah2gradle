package com.example.trainingSpringBootJpaH2Gradle.service;

import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.Color;
import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.ItemMarketing;

import java.util.List;

public interface ItemMarketingService {

    ItemMarketing saveItemMarketing(ItemMarketing itemMarketing);

    List<ItemMarketing> getItemMarketings();

    ItemMarketing getItemMarketing(Integer id);

    ItemMarketing updateItemMarketing(ItemMarketing itemMarketing);

    void deleteItemMarketing(Integer itemMarketingId);

}
