package com.example.trainingSpringBootJpaH2Gradle.service;

import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.Item;
import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.ItemMarketing;

import java.util.List;

public interface ItemService {

    Item saveItem(Item item);

    List<Item> getItems();

    Item getItem(Integer id);

    Item updateItem(Item item);

    void deleteItem(Integer itemId);

}
