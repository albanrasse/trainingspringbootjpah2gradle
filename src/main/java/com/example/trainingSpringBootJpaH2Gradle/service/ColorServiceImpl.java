package com.example.trainingSpringBootJpaH2Gradle.service;

import com.example.trainingSpringBootJpaH2Gradle.model.Company;
import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.Color;
import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.Size;
import com.example.trainingSpringBootJpaH2Gradle.repository.ColorRepository;
import com.example.trainingSpringBootJpaH2Gradle.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ColorServiceImpl implements ColorService {

    @Autowired
    private ColorRepository colorRepository;

    public Color save(Color size){
        try {
            size = colorRepository.save(size);
        }catch(DataIntegrityViolationException e){
            System.out.println("ERROR : Unique constraint violation " + size.toString());
        }
        return size;
    }

    public List<Color> findAll(){
        return (List<Color>) colorRepository.findAll();
    }

    public Color findById(Integer id){
        return (Color) colorRepository.findById(id).get();
    }

    public void delete(Color color){
        colorRepository.delete(color);
    }

    @Override
    public Color saveColor(Color color) {
        return null;
    }

    @Override
    public List<Color> getColors() {
        return null;
    }

    @Override
    public Color getColor(Integer id) {
        return null;
    }

    @Override
    public Color updateColor(Color color) {
        return null;
    }

    @Override
    public void deleteColor(Integer colorId) {

    }
}
