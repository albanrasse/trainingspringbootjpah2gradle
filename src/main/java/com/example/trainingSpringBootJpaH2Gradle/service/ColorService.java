package com.example.trainingSpringBootJpaH2Gradle.service;

import com.example.trainingSpringBootJpaH2Gradle.model.Company;
import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.Color;

import java.util.List;

public interface ColorService {

    Color saveColor(Color color);

    List<Color> getColors();

    Color getColor(Integer id);

    Color updateColor(Color color);

    void deleteColor(Integer colorId);

}
