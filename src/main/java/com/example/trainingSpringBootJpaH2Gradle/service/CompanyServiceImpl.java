package com.example.trainingSpringBootJpaH2Gradle.service;

import com.example.trainingSpringBootJpaH2Gradle.model.Company;
import com.example.trainingSpringBootJpaH2Gradle.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyServiceImpl implements CompanyService {


    @Autowired
    private CompanyRepository companyRepository;

    public Company save(Company company){
        try {
            company = companyRepository.save(company);
        }catch(DataIntegrityViolationException e){
            System.out.println("ERROR : Unique constraint violation " + company.toString());
        }
        return company;
    }

    public List<Company> findAll(){
        return (List<Company>) companyRepository.findAll();
    }

    public Company findById(Integer id){
        return (Company) companyRepository.findById(id).get();
    }

    public void delete(Company company){
        companyRepository.delete(company);
    }

    @Override
    public Company saveCompany(Company department) {
        return null;
    }

    @Override
    public List<Company> getCompanies() {
        return null;
    }

    @Override
    public Company getCompany(Integer id) {
        return null;
    }

    @Override
    public Company updateCompany(Company company) {
        return null;
    }

    @Override
    public void deleteCompany(Integer departmentId) {

    }
}
