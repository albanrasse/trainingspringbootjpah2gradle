package com.example.trainingSpringBootJpaH2Gradle.service;

import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.Color;
import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.Vendor;
import com.example.trainingSpringBootJpaH2Gradle.repository.ColorRepository;
import com.example.trainingSpringBootJpaH2Gradle.repository.VendorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VendorServiceImpl implements VendorService {

    @Autowired
    private VendorRepository vendorRepository;

    public Vendor save(Vendor vendor){
        try {
            vendor = vendorRepository.save(vendor);
        }catch(DataIntegrityViolationException e){
            System.out.println("ERROR : Unique constraint violation " + vendor.toString());
        }
        return vendor;
    }

    public List<Vendor> findAll(){
        return (List<Vendor>) vendorRepository.findAll();
    }

    public Vendor findById(String id){
        return (Vendor) vendorRepository.findById(id).get();
    }

    public void delete(Vendor vendor){
        vendorRepository.delete(vendor);
    }

    @Override
    public Vendor saveVendor(Vendor color) {
        return null;
    }

    @Override
    public List<Vendor> getVendors() {
        return null;
    }

    @Override
    public Vendor getVendor(String id) {
        return null;
    }

    @Override
    public Vendor updateVendor(Vendor vendor) {
        return null;
    }

    @Override
    public void deleteVendor(String id) {

    }
}
