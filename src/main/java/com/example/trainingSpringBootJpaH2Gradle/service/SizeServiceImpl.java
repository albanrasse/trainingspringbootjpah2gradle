package com.example.trainingSpringBootJpaH2Gradle.service;

import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.Size;
import com.example.trainingSpringBootJpaH2Gradle.repository.SizeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SizeServiceImpl implements SizeService {


    @Autowired
    private SizeRepository sizeRepository;

    public Size save(Size size){
        try {
            size = sizeRepository.save(size);
        }catch(DataIntegrityViolationException e){
            System.out.println("ERROR : Unique constraint violation " + size.toString());
        }
        return size;
    }

    public List<Size> findAll(){
        return (List<Size>) sizeRepository.findAll();
    }

    public Size findById(Integer id){
        return (Size) sizeRepository.findById(id).get();
    }

    public void delete(Size size){
        sizeRepository.delete(size);
    }

    @Override
    public Size saveSize(Size size) {
        return null;
    }

    @Override
    public List<Size> getSizes() {
        return null;
    }

    @Override
    public Size getSize(Integer id) {
        return null;
    }

    @Override
    public Size updateSize(Size size) {
        return null;
    }

    @Override
    public void deleteSize(Integer sizeId) {

    }
}
