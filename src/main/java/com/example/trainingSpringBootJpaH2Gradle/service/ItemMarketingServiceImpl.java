package com.example.trainingSpringBootJpaH2Gradle.service;

import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.Color;
import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.ItemMarketing;
import com.example.trainingSpringBootJpaH2Gradle.repository.ColorRepository;
import com.example.trainingSpringBootJpaH2Gradle.repository.ItemMarketingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemMarketingServiceImpl implements ItemMarketingService {

    @Autowired
    private ItemMarketingRepository itemMarketingRepository;

    public ItemMarketing save(ItemMarketing itemMarketing){
        try {
            itemMarketing = itemMarketingRepository.save(itemMarketing);
        }catch(DataIntegrityViolationException e){
            System.out.println("ERROR : Unique constraint violation " + itemMarketing.toString());
        }
        return itemMarketing;
    }

    public List<ItemMarketing> findAll(){
        return (List<ItemMarketing>) itemMarketingRepository.findAll();
    }

    public ItemMarketing findById(Integer id){
        return (ItemMarketing) itemMarketingRepository.findById(id).get();
    }

    public void delete(ItemMarketing itemMarketing){
        itemMarketingRepository.delete(itemMarketing);
    }

    @Override
    public ItemMarketing saveItemMarketing(ItemMarketing itemMarketing) {
        return null;
    }

    @Override
    public List<ItemMarketing> getItemMarketings() {
        return null;
    }

    @Override
    public ItemMarketing getItemMarketing(Integer id) {
        return null;
    }

    @Override
    public ItemMarketing updateItemMarketing(ItemMarketing itemMarketing) {
        return null;
    }

    @Override
    public void deleteItemMarketing(Integer itemMarketingId) {

    }
}
