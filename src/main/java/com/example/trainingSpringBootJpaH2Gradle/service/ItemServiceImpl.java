package com.example.trainingSpringBootJpaH2Gradle.service;

import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.Item;
import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.ItemMarketing;
import com.example.trainingSpringBootJpaH2Gradle.repository.ItemMarketingRepository;
import com.example.trainingSpringBootJpaH2Gradle.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemRepository itemRepository;

    public Item save(Item item){
        try {
            item = itemRepository.save(item);
        }catch(DataIntegrityViolationException e){
            System.out.println("ERROR : Unique constraint violation " + item.toString());
        }
        return item;
    }

    public List<Item> findAll(){
        return (List<Item>) itemRepository.findAll();
    }

    public Item findById(Integer id){
        return (Item) itemRepository.findById(id).get();
    }

    public void delete(Item item){
        itemRepository.delete(item);
    }

    @Override
    public Item saveItem(Item item) {
        return null;
    }

    @Override
    public List<Item> getItems() {
        return null;
    }

    @Override
    public Item getItem(Integer id) {
        return null;
    }

    @Override
    public Item updateItem(Item item) {
        return null;
    }

    @Override
    public void deleteItem(Integer itemId) {

    }
}
