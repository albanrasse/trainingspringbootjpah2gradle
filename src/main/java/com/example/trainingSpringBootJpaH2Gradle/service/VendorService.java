package com.example.trainingSpringBootJpaH2Gradle.service;

import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.Color;
import com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel.Vendor;

import java.util.List;

public interface VendorService {

    Vendor saveVendor(Vendor color);

    List<Vendor> getVendors();

    Vendor getVendor(String id);

    Vendor updateVendor(Vendor vendor);

    void deleteVendor(String colorId);

}
