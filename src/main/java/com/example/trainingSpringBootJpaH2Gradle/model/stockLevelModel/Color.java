package com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;

import javax.persistence.*;

@Entity
public class Color {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    private String code;

    private String label;

    public Color(String code, String label){
        this.code = code;
        this.label = label;
    }

}
