package com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel;

import javax.persistence.*;

@Entity
public class Vendor {

    @Id
    @Column(unique = true)
    private String id;
    private String name;

    public Vendor(String id, String name){
        this.id = id;
        this.name = name;
    }

    public Vendor(){}

}
