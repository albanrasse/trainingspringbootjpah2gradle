package com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel;

import javax.persistence.*;

@Entity
public class Item {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String axItemId;
    private String as400ItemId;
    private String productName;
    @OneToOne
    private ItemMarketing itemMarketing;
    @OneToOne
    private Vendor vendor;

    public Item(String axItemId, String as400ItemId, String productName, ItemMarketing itemMarketing){
        this.axItemId = axItemId;
        this.as400ItemId = as400ItemId;
        this.productName = productName;
        this.itemMarketing = itemMarketing;
    }

    public Item(String axItemId, String as400ItemId, String productName, ItemMarketing itemMarketing, Vendor vendor){
        this.axItemId = axItemId;
        this.as400ItemId = as400ItemId;
        this.productName = productName;
        this.itemMarketing = itemMarketing;
        this.vendor = vendor;
    }

}
