package com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel;

import javax.persistence.*;

@Entity
public class ItemMarketing {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private int indicator;
    @OneToOne
    private Color color;
    @OneToOne
    private Size size;
    @OneToOne
    private Item replacementItem;
    @OneToOne
    private Item substituteItem;

    public ItemMarketing(int indicator, Color color, Size size){
        this.indicator = indicator;
        this.color = color;
        this.size = size;
    }

    public ItemMarketing(int indicator, Color color, Size size, Item replacementItem, Item substituteItem){
        this.indicator = indicator;
        this.color = color;
        this.size = size;
        this.replacementItem = replacementItem;
        this.substituteItem = substituteItem;
    }

}
