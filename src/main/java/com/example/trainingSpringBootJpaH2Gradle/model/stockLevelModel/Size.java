package com.example.trainingSpringBootJpaH2Gradle.model.stockLevelModel;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Size {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    private String code;

    private String label;

    public Size(String code, String label){
        this.code = code;
        this.label = label;
    }
}
